import React from "react";

function Footer(props) {
  return (
    <footer>
      <div className="tops">
        <div className="socials">
          <i className="fab fa-facebook-f" />
          <i className="fab fa-linkedin" />
          <i className="fab fa-linkedin-in" />
        </div>
        <ul className="links">
          <li>Terms and Condition</li>
          <li>Privacy Policy</li>
          <li>Google Policy</li>
        </ul>
        <div className="contact">
          <div className="icon">
            <i className="fas fa-headset" />
          </div>
          <div className="texts">
            <p className="sm">Need help?</p>
            <p className="bg">Please contact support</p>
          </div>
        </div>
      </div>
      <div className="copyright-text">
        {" "}
        <p className="copy">
          Virtual text | A full version of the website All Rights Reserved
        </p>
      </div>
    </footer>
  );
}

export default Footer;
