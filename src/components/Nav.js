import React from 'react';

function Nav(props) {
    return (
        <nav className="main-nav">
  <div className="leftmenu">
    <div className="logo">
      <img src="/afterpay-logo-png-black-transparent-860x166.png" alt />
    </div>
    <ul className="menu">
      <a href><li className="menu-links">Home</li></a>
      <a href><li className="menu-links">About us</li></a>
      <a href><li className="menu-links">Portfolio</li></a>
      <a href><li className="menu-links">News</li></a>
      <a href><li className="menu-links">Location</li></a>
    </ul>
  </div>
  <div className="rightmenu">
    <div className="searchbox">
      <input type="search" name id className="search" />
    </div>
    <div className="logintext">Login</div>
    <div className="signinbtn">Sign Up</div>
  </div>
</nav>

    );
}

export default Nav;