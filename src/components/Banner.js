import React from "react";

function Banner(props) {
  return (
    <div className="main-banner">
      <div className="top-inner">
        <h1 className="toptext">Rolobank</h1>
        <h1 className="toptext blue-color">
          We are a <br />
          bank of the young and for the young
        </h1>
      </div>
    </div>
  );
}

export default Banner;
