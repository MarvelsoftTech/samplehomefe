import React, { useState } from "react";
import LatestNewsCard from "./LatestNewsCard";

function LatestNew(props) {
  return (
    <>
      <div className="wrapper">
        <h1 className="lat">Latest News</h1>
        <p className="sub">Catch all the information on time here</p>
        <div className="pagearrow">
          <i className="fas fa-chevron-left" />
          <i className="fas fa-chevron-right current" />
        </div>
      </div>
      <div className="container-fluid">
        <div className="row posts">
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
          <LatestNewsCard />
        </div>
      </div>
    </>
  );
}

export default LatestNew;
