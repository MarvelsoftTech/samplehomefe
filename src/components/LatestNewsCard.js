import React from "react";

function LatestNewsCard(props) {
  return (
    <>
      <div className="col-lg-3">
        <div className="imagebox">
          <img src alt />
        </div>
        <h4 className="post-title">Centenerians Day</h4>
        <p className="post-snip">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium
          sint minima hic recusandae magni!
        </p>
        <span className="pre">premium</span>
      </div>
    </>
  );
}

export default LatestNewsCard;
