import React from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';
import LatestNew from '../components/LatestNew';
import Nav from '../components/Nav';

function Home(props) {
    return (
        <>
            <Nav/>
            <Banner/>
            <LatestNew/>
            <Footer/>
        </>
    );
}

export default Home;